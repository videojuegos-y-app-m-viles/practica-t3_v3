package com.android.practica_t3_3v;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.android.practica_t3_3v.entities.Pokemon;
import com.android.practica_t3_3v.services.IPokemonServices;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Activity_CrearPokemon extends AppCompatActivity {

    private EditText edtNombre;
    private EditText edtTipo;
    private EditText edtImage;
    private EditText edtLatitud;
    private EditText edtLongitud;

    Button crear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_pokemon);

        edtNombre = findViewById(R.id.edtxtNombre);
        edtTipo = findViewById(R.id.edtxtTipo);
        edtImage = findViewById(R.id.edtxtImagen);

        edtLatitud = findViewById(R.id.edtxtLatitud);
        edtLongitud = findViewById(R.id.edtxtLongitud);

        crear = findViewById(R.id.btnCrear);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        IPokemonServices services = retrofit.create(IPokemonServices.class);

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = edtNombre.getText().toString();
                String tipo = edtTipo.getText().toString();
                String image = edtImage.getText().toString();
                double latitud = Double.parseDouble(edtLatitud.getText().toString());
                double longitud = Double.parseDouble(edtLongitud.getText().toString());


                Pokemon pokemon = new Pokemon();
                pokemon.setNombrePokemon(nombre);
                pokemon.setTipoP(tipo);
                pokemon.setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png");
                pokemon.setLatitud(latitud);
                pokemon.setLongitud(longitud);


                services.create(pokemon).enqueue(new Callback<Pokemon>() {
                    @Override
                    public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {
                        int valor = response.code();
                        //Toast.makeText(getApplicationContext(), "Codigo"+response.code() , Toast.LENGTH_SHORT).show();
                        Log.i("MAIN_APP",new Gson().toJson(response.body()));
                    }

                    @Override
                    public void onFailure(Call<Pokemon> call, Throwable t) {

                    }
                });
            }
        });

    }
}