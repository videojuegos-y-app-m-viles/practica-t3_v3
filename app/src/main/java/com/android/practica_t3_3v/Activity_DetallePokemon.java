package com.android.practica_t3_3v;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Activity_DetallePokemon extends AppCompatActivity {

    private TextView txtNombre;
    private TextView txtTipo;
    private ImageView txtImage;
    private double txtLatitud;
    private double txtLongitud;

    private Button btnUbicacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pokemon);

        Intent intent = getIntent();

        String nombre = intent.getStringExtra("Nombre");
        String tipo = intent.getStringExtra("Tipo");
        String image = intent.getStringExtra("Imagen");
        String latitud = intent.getStringExtra("Latitud");
        String longitud = intent.getStringExtra("Longitud");

        txtNombre = findViewById(R.id.txtNombre);
        txtTipo = findViewById(R.id.txtTipo);
        txtImage = findViewById(R.id.imagen);
        btnUbicacion = findViewById(R.id.verUbicacion);


        Picasso.get().load(image).resize (250, 250).into(txtImage);

        txtNombre.setText("Nombre: "+ nombre);
        txtTipo.setText("Tipo: " + tipo);


        btnUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ubicacion = new Intent(getApplicationContext(),MapsActivity.class);
                ubicacion.putExtra("Latitud",latitud);
                ubicacion.putExtra("Longitud",longitud);

                startActivity(ubicacion);
            }
        });

    }
}