package com.android.practica_t3_3v;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;

import com.android.practica_t3_3v.adapters.PokemonAdapter;
import com.android.practica_t3_3v.entities.Pokemon;
import com.android.practica_t3_3v.services.IPokemonServices;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Activity_MuestraResultados extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muestra_resultados);

        RecyclerView rcvPokemons= findViewById(R.id.rcvResultados);
        rcvPokemons.setHasFixedSize(true);
        rcvPokemons.setLayoutManager(new LinearLayoutManager(this));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        List<Pokemon> listaTemporal = new ArrayList<>();
        Pokemon picachu = new Pokemon();
        picachu.setNombrePokemon("Charizar");
        picachu.setTipoP("Fuego");
        picachu.setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png");
        picachu.setLatitud(10);
        picachu.setLongitud(10);
        listaTemporal.add(picachu);

        Pokemon pokemon = new Pokemon();
        picachu.setNombrePokemon("Pucachu");
        picachu.setTipoP("Eléctrico");
        picachu.setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png");
        picachu.setLatitud(10);
        picachu.setLongitud(10);
        listaTemporal.add(picachu);

        PokemonAdapter adapter = new PokemonAdapter(listaTemporal);

        pokemon = new Pokemon();
        picachu.setNombrePokemon("Charizar");
        picachu.setTipoP("Fuego");
        picachu.setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png");
        picachu.setLatitud(10);
        picachu.setLongitud(10);
        listaTemporal.add(picachu);

        pokemon = new Pokemon();
        picachu.setNombrePokemon("Charizar");
        picachu.setTipoP("Fuego");
        picachu.setImage("https://assets.pokemon.com/assets//cms2-es-es/img/watch-pokemon-tv/_tiles/broadcaster/season23-boing-169.png");
        picachu.setLatitud(10);
        picachu.setLongitud(10);
        listaTemporal.add(picachu);

        pokemon = new Pokemon();
        picachu.setNombrePokemon("Charizar");
        picachu.setTipoP("Fuego");
        picachu.setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png");
        picachu.setLatitud(10);
        picachu.setLongitud(10);
        listaTemporal.add(picachu);
        rcvPokemons.setAdapter(adapter);

        /*
        IPokemonServices services = retrofit.create(IPokemonServices.class);
        services.allPokemons().enqueue(new Callback<List<Pokemon>>() {
            @Override
            public void onResponse(Call<List<Pokemon>> call, Response<List<Pokemon>> response) {
                PokemonAdapter adapter = new PokemonAdapter(response.body());
                rcvPokemons.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Pokemon>> call, Throwable t) {

            }
        });
         */
    }
}