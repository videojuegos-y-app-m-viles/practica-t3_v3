package com.android.practica_t3_3v.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.practica_t3_3v.Activity_DetallePokemon;
import com.android.practica_t3_3v.R;
import com.android.practica_t3_3v.entities.Pokemon;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PokemonAdapter  extends RecyclerView.Adapter<PokemonAdapter.ViewHolder> {

    private List<Pokemon> data;

    public PokemonAdapter(List<Pokemon> data){
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView tvName = holder.itemView.findViewById(R.id.tvNamePokemon);
        TextView tvTipo = holder.itemView.findViewById(R.id.tvTipo);

        ImageView iv = holder.itemView.findViewById(R.id.ivImage);
        Button btnVerDetalle = holder.itemView.findViewById(R.id.btnVerDetalle);

        Pokemon pokemon = data.get(position);

        tvName.setText(pokemon.getNombrePokemon());
        tvTipo.setText(pokemon.getTipoP());
        String imagen = pokemon.getImage();

        Picasso.get().load(imagen).resize (250, 250).into(iv);

        btnVerDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detail = new Intent(view.getContext(), Activity_DetallePokemon.class);
                detail.putExtra("Nombre",pokemon.getNombrePokemon());
                detail.putExtra("Tipo",pokemon.getTipoP());
                detail.putExtra("Imagen",pokemon.getImage());
                detail.putExtra("Latitud",pokemon.getLatitud());
                detail.putExtra("Longitud",pokemon.getLongitud());
                view.getContext().startActivity(detail);
            }

        });


    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View itemView) {
            super(itemView);
        }

    }
}