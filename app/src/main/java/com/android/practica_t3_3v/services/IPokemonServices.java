package com.android.practica_t3_3v.services;

import com.android.practica_t3_3v.entities.Pokemon;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IPokemonServices {

    @GET("pokemons/n00184115")
    Call<List<Pokemon>> allPokemons();

    @POST("pokemons/n00184115/crear")
    Call<Pokemon> create(@Body Pokemon pokemon);

}
